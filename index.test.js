const server = require('./dist/index.js');
const supertest = require('supertest');
const requestWithSupertest = supertest(server);


describe('Default endpoint', () => {

  it('GET / should return hello', async () => {
    const res = await requestWithSupertest.get('/');
      expect(res.status).toEqual(200);
      console.log(res.text)
      expect(res.text).toEqual('Hello CICD v2!!!');
      // expect(res.body).toHaveProperty('users')
  });

  xit('GET /login should return a login page', async () => {
    const res = await requestWithSupertest.get('/login');
      expect(res.status).toEqual(200);
      console.log(res.text)
      expect(res.text).toEqual('Login Page');
  });

  afterAll(() => {
    server.close();
  });

});